using System.Collections.Generic;
using UnityEngine;

public class SymbolContent : MonoBehaviour
{
    [SerializeField] private Transform content;
    [SerializeField] private SymbolItem symbolItem;
    private RectTransform rectTransform;
    private float contentWidth;
    private Pool<SymbolItem> poolSymols;
    private char[] letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
    void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        contentWidth = rectTransform.rect.width;
        poolSymols = new Pool<SymbolItem>(Instantiate, content, symbolItem.gameObject);        
    }
    private void Start()
    {
        //FillingRandomeSymbol(5, 5);
    }
    public void Move()
    {
        List<SymbolItem> symbols = new List<SymbolItem>(poolSymols.GetActiveObjects());
        if (symbols.Count <= 1)
            return;

        foreach (var symbol in symbols)
        {
            if (symbol.OnMove)
                return;
        }

        while(symbols.Count > 1)              
        {
            int i = symbols.Count - 1;
            int id = i;
            while (id == i && symbols.Count > 1)
            {
                id = Random.Range(0, symbols.Count);
            }
            var symbol1 = symbols[i];
            var symbol2 = symbols[id];
            symbol1.MoveOnPosition(symbol2.StartPosition);
            symbol2.MoveOnPosition(symbol1.StartPosition);
            symbols.RemoveAt(i);
            symbols.RemoveAt(id);
        }
        if(symbols.Count==1)
        {
            var symbol1 = symbols[0];
            List<SymbolItem> poolSymbols = poolSymols.GetActiveObjects();
            int id = 0; 
            var symbol2 = poolSymbols[id];
            while(symbol1 == symbol2)
            {
                id = Random.Range(0, poolSymbols.Count);
                symbol2 = poolSymbols[id];
            }
            symbol1.MoveOnPosition(symbol2.FinishingPosition);
            symbol2.MoveOnPosition(symbol1.StartPosition);
        }
    }
    public void FillingRandomeSymbol(int width, int height)
    {
        poolSymols.ClearAll();
        float size = 0f;
        if(width>height)
            size = contentWidth / width;
        else
            size = contentWidth / height;

        float halfSize = size / 2;
        float shiftX = size * width / 2f;
        float shiftY = size * height / 2f;
        for (int i = 0; i < height; i++)
        {
            for (int l = 0; l < width; l++)
            {
                SymbolItem symbol = poolSymols.Get();
                symbol.Place(l * size + halfSize - shiftX, i * size + halfSize- shiftY, size, size);
                symbol.SetChar(GetRandomSymbol());
            }
        }
    }
    private char GetRandomSymbol()
    {
        return letters[Random.Range(0, letters.Length)];
    }
}
