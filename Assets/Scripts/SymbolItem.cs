using System;
using System.Collections;
using TMPro;
using UnityEngine;

public class SymbolItem : MonoBehaviour
{
    [SerializeField] private float timeMove = 2f; //����� ����� ������� � ��������
    private TextMeshProUGUI text;
    private RectTransform rectTransform;    
    private Coroutine moveCoroutine;
    public Vector2 StartPosition { get; private set; }    
    public Vector2 FinishingPosition { get; private set; }
    public bool OnMove { get; private set; } = false;
    private void Awake()
    {
        text = GetComponent<TextMeshProUGUI>();
        rectTransform = GetComponent<RectTransform>();
    }
    public void Place(float x, float y, float width, float height)
    {
        StopAllCoroutines();
        OnMove = false;
        rectTransform.sizeDelta = new Vector2(width, height);
        StartPosition = new Vector2(x, y);
        SetPosition(new Vector2(x, y));
    }
    private void SetPosition(Vector2 position)
    {
        rectTransform.anchoredPosition = position;        
    }
    public void MoveOnPosition(Vector2 position)
    {
        if (OnMove)
            StopCoroutine(moveCoroutine);        

        FinishingPosition = position;
        moveCoroutine = StartCoroutine(CMoveOnPosition(position));
    }    

    private IEnumerator CMoveOnPosition(Vector2 position)
    {
        OnMove = true;
        float delimer = 100f;
        Vector2 startPos = rectTransform.anchoredPosition;
        float stepTime = timeMove / delimer; 
        float lerpValue = 0f;
        while (lerpValue < 1)
        {
            lerpValue += 1 / delimer;
            float newX = Mathf.Lerp(startPos.x, position.x, lerpValue);
            float newY = Mathf.Lerp(startPos.y, position.y, lerpValue);
            SetPosition(new Vector2(newX, newY));
            yield return new WaitForSeconds(stepTime);
        }
        SetPosition(FinishingPosition);
        StartPosition = FinishingPosition;
        OnMove = false;
        yield return null;
    }
    public void SetChar(char value)
    {
        text.text = value.ToString();
    }
}
