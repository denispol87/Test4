using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


public class Menu : MonoBehaviour
{
    [SerializeField] private Button generateButton;
    [SerializeField] private Button moveButton;
    [SerializeField] private TMP_InputField widthInput;
    [SerializeField] private TMP_InputField heightInput;

    [SerializeField] private SymbolContent symbolContent;
    private void Start()
    {
        generateButton.onClick.AddListener(PressGenerate);
        moveButton.onClick.AddListener(Move);
        widthInput.onEndEdit.AddListener((value)=> { CheckInputText(value, widthInput); });
        heightInput.onEndEdit.AddListener((value)=> { CheckInputText(value, heightInput); });
    }
    private void CheckInputText(string value, TMP_InputField inputField)
    {
        if (value != "")
        {
            value = Mathf.Abs(int.Parse(value)).ToString();
            inputField.SetTextWithoutNotify(value);
        }
    }
    private void Move()
    {
        symbolContent.Move();
    }
    private void PressGenerate()
    {
        int width = 0;
        int height = 0;
        if (widthInput.text != "")
            width = int.Parse(widthInput.text);

        if (heightInput.text != "")
            height = int.Parse(heightInput.text);

        symbolContent.FillingRandomeSymbol(width, height);
    }
}
